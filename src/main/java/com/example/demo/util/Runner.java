package com.example.demo.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Runner {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper mapper;
	
	private static final Log logger = LogFactory.getLog(Runner.class);
	
	@PostConstruct
	public void start() {
		logger.info("starting.....");
		Map<String, Long> responseMap = CSVReaderUtil.loadMap();
		logger.info("map loaded.....");
		List<APIBody> apiMessageList = CSVReaderUtil.getAPIBody();
		logger.info("api body list created size is "+apiMessageList.size());
		int currentCount = 0;
		for (APIBody apiBody : apiMessageList) {
			Long res = sendRequest(apiBody);
			currentCount++;

			try {
				String compId = apiBody.getCompanyId();
				if (!responseMap.containsKey(compId)) {
					responseMap.put(compId, res);
				}
				if (responseMap.containsKey(compId) && responseMap.get(compId) > res) {
					responseMap.put(compId, res);
				}
				if(currentCount%1000==0) {
					logger.info("saving intermediate response map to file.....so far "+currentCount+" request done ");
					CSVReaderUtil.SaveToFile(responseMap);
				}
			} catch (Exception e) {
				logger.error("Error occured while creating map", e );
			}
		}
		logger.info("saving map to disc....");
		CSVReaderUtil.SaveToFile(responseMap);
		logger.info("process finished....");
	}

	private Long sendRequest(APIBody apiBody) {
		// String url = "http://service.sm.resdex.com/restApi/postJob";
		String url = "http://dummy.restapiexample.com/api/v1/create";
		Long res = Long.MAX_VALUE;
		String apiResponse = null;
		try {
			String data = mapper.writeValueAsString(apiBody);
			apiResponse = restTemplate.postForObject(url, data, String.class);
			String compId = apiBody.getCompanyId();
			res = Long.parseLong(apiResponse);
		} catch (Exception e) {
			logger.error("Error occured while sending request : ", e);
		}
		return res;
	}
}
