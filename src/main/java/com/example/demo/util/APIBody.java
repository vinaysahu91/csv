package com.example.demo.util;

import java.util.List;

public class APIBody {

	private String clientId;// compamny_id
	private String jobId; // file
	private String companyId;// compamny_id
	private String recruiterId; // username
	private String questionnaireId;
	private String projectId;
	private String projectName;
	private List<String> subUserList;

	public APIBody() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public APIBody(String clientId, String jobId, String companyId, String recruiterId, String questionnaireId,
			String projectId, List<String> subUserList) {
		super();
		this.clientId = clientId;
		this.jobId = jobId;
		this.companyId = companyId;
		this.recruiterId = recruiterId;
		this.questionnaireId = questionnaireId;
		this.projectId = projectId;
		this.subUserList = subUserList;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getRecruiterId() {
		return recruiterId;
	}

	public void setRecruiterId(String recruiterId) {
		this.recruiterId = recruiterId;
	}

	public String getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(String questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public List<String> getSubUserList() {
		return subUserList;
	}

	public void setSubUserList(List<String> subUserList) {
		this.subUserList = subUserList;
	}

}
