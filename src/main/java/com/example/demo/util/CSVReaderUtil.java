package com.example.demo.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opencsv.CSVReader;

public class CSVReaderUtil {

	private static final String filePath = "data.csv";
	private static final String resFilePath = "response.csv";
	
	private static final Log logger = LogFactory.getLog(CSVReaderUtil.class);
	
	public static List<APIBody> getAPIBody() {
		logger.info("creating api body.....");
		List<APIBody> apiBodyList = new ArrayList<APIBody>();
		CSVReader csvReader = null;
		try {
			FileReader filereader = new FileReader(filePath);
			csvReader = new CSVReader(filereader);
			String[] nextRecord;
			int curr = 0;
			while ((nextRecord = csvReader.readNext()) != null) {
				if(curr%1000==0) {
					logger.info(apiBodyList.size()+" record added to map");
				}
				String jobId = nextRecord[0];
				String projectName = nextRecord[1];
				String clientId = nextRecord[2];
				String recruiterId = nextRecord[3];
				APIBody apiBody = new APIBody();
				apiBody.setJobId(jobId);
				apiBody.setProjectName(projectName);
				apiBody.setClientId(clientId);
				apiBody.setRecruiterId(recruiterId);
				apiBody.setCompanyId(clientId);
				apiBodyList.add(apiBody);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				csvReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return apiBodyList;
	}

	public static Map<String, Long> loadMap() {
		Map<String, Long> resMap = new HashMap<String, Long>();
		CSVReader csvReader = null;
		try {
			FileReader filereader = new FileReader(resFilePath);
			csvReader = new CSVReader(filereader);
			String[] nextRecord;

			while ((nextRecord = csvReader.readNext()) != null) {
				String compId = nextRecord[0];
				Long projId = Long.valueOf(nextRecord[1]);
				resMap.put(compId, projId);
			}
		} catch (Exception e) {
			return resMap;
		} finally {
			try {
				if (csvReader != null)
					csvReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return resMap;
	}

	public static void SaveToFile(Map<String, Long> content) {
		
		 File file = new File(resFilePath);
		 if (file.exists()){
		     file.delete();
		 } 
		 
		String strContent = getContent(content);
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(resFilePath));
			writer.write(strContent);

		} catch (IOException e) {
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
			}
		}
	}

	private static String getContent(Map<String, Long> content) {
		String strContent = "";
		for (Entry<String, Long> entry : content.entrySet()) {
			strContent += entry.getKey() + "," + entry.getValue() + "\n";
		}
		return strContent;
	}
}
